#!/usr/bin/env python3
"""
Downloads all images from websites. The websites are specified by a list of 
urls. The downloaded images are placed in the directory 'output', unless 
otherwise specified. 

:param urls: The urls of websites to download all images from.
:param -o --output-dir: The directory to place the downloaded images. Default is \
`output`.

Example usage:
Download all images from http://mywebsite.com/ and put them in the directory
'output'.
./img-dl.py http://mywebsite.com/

Download all images from http://mywebsite.com/ and put them in the directory
'mydir'.
./img-dl.py https://mywebsite.com/ --output-dir mydir
"""

import requests
import argparse
from bs4 import BeautifulSoup

import os
import urllib.parse
import re

DEFAULT_OUTPUT_DIR = 'output'
HTTP_PATTERN = re.compile('https?')

def init_arg_parser():
	parser = argparse.ArgumentParser()
	parser.add_argument('urls', 
		nargs='+', 
		help='The urls of websites to download all images from.')
	parser.add_argument('-o', '--output-dir', 
		default = DEFAULT_OUTPUT_DIR,
		help='The directory to place the downloaded images. Default is \
		`output`.')
	return parser

def create_output_dir(output_dir):
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

def fetch_all(urls, output_dir):
	for url in urls:
		resp = requests.get(url)
		soup = BeautifulSoup(resp.text, 'html.parser')
		img_tags = soup.find_all('img')

		img_urls = map(lambda img_tag: img_tag['src'], img_tags)
		img_urls = map(lambda img_url: absolute_url(url, img_url), img_urls)

		fetch_all_images(img_urls, output_dir)

def absolute_url(url, relative):
	if HTTP_PATTERN.match(relative):
		return relative
	split = urllib.parse.urlsplit(url)
	if relative.startswith('//'):
		return '{}:{}'.format( split.scheme, relative)
	base = '{}://{}'.format( split.scheme, split.netloc.rstrip('/') )
	path = relative.lstrip('/')
	return '/'.join( [base, relative] )

def fetch_all_images(img_urls, output_dir):
	for url in img_urls:
		split = urllib.parse.urlsplit(url)
		filename = os.path.basename(split.path)
		path = os.path.join(output_dir, filename)

		resp = requests.get(url, stream=True)
		with open(path, 'wb') as img_file:
			for part in resp.iter_content(chunk_size=128):
				img_file.write(part)

def main():
	parser = init_arg_parser()
	args = parser.parse_args()
	create_output_dir(args.output_dir)
	fetch_all(args.urls, args.output_dir)

if __name__ == '__main__':
	main()

