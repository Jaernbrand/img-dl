# img-dl
Downloads all images from websites. The websites are specified by a list of 
urls. The downloaded images are placed in the directory 'output', unless 
otherwise specified. 

## Requirements
### [requests](http://docs.python-requests.org/en/master/)
```
pip install requests
```
### [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)
```
pip install beautifulsoup4
```

## Usage
* __urls__: The urls of websites to download all images from.
* __-o__ __--output-dir__: The directory to place the downloaded images. 
Default is `output`.

Example usage:
Download all images from http://mywebsite.com/ and put them in the directory
'output'.
```
./img-dl.py http://mywebsite.com/
```

Download all images from http://mywebsite.com/ and put them in the directory
'mydir'.
```
./img-dl.py https://mywebsite.com/ --output-dir mydir
```

